$(document).ready(function() {
	$('ul#filter a').click(function() {
		$(this).css('outline','none');
		$('ul#filter .current').removeClass('current');
		$(this).parent().addClass('current');
		var filterVal = $(this).text().toLowerCase().replace(' ','-');
		if(filterVal == 'all') {
			$('.team_box.hidden').show().removeClass('hidden');
		} else {
			$('.team_box').each(function() {
				if(!$(this).hasClass(filterVal)) {
					$(this).hide().addClass('hidden');
				} else {
					$(this).show().removeClass('hidden');
				}
			});
		}
		return false;
	});
});

$(document).ready(function() {
	$('ul#filter a').click(function() {
		$(this).css('outline','none');
		$('ul#filter .current').removeClass('current');
		$(this).parent().addClass('current');
		var filterVal = $(this).text().toLowerCase().replace(' ','-');
		if(filterVal == 'all') {
			$('.team_box2.hidden').show().removeClass('hidden');
		} else {
			$('.team_box2').each(function() {
				if(!$(this).hasClass(filterVal)) {
					$(this).hide().addClass('hidden');
				} else {
					$(this).show().removeClass('hidden');
				}
			});
		}
		return false;
	});
});