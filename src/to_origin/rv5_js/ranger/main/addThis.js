$(document).ready(function(){
	if(typeof addthis != "undefined"){
		addthis.layers({
			'theme' : 'transparent',
			'share' : {
				'position' : 'left',
				'services' : 'facebook,twitter,google_plusone_share,pinterest',
				'offset': {
					'top':'200px'
				}
			},
			'thankyou' : 'false'
		});
	} 
});
