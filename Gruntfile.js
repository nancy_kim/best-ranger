module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        jslint: {
            client: {
                src: [
                    'src/_js/**/*.js'
                ],
                directives: {
                    browser: true
                },
                options: {

                }
            }
        },
        jshint: {
            all: [
                'src/_js/**/*.js'
            ]
        },
        sass: {
            dist: {
                options: {
                    style: 'compressed'
                },
                files: {
                    'src/to_origin/rv5_css/ranger/style.css': 'src/_scss/ranger/style.scss'
                }

            }
        },
        jsbeautifier: {
            files: ['src/**/*.html'],
            options: {}
        },
        accessibility: {
            options: {
                accessibilityLevel: 'WCAG2A'
            },
            test: {
                src: ['src/**/*.html']
            }
        },
        curl: {
            'build/rv5_images.zip': 'https://bitbucket.org/armydotmil/<%= pkg.name %>/downloads/rv5_images.zip',
            'build/rv5_downloads.zip': 'https://bitbucket.org/armydotmil/<%= pkg.name %>/downloads/rv5_downloads.zip'
        },
        zip: {
            'build/rv5_images.zip': ['src/to_origin/rv5_images/**/*'],
            'build/rv5_downloads.zip': ['src/to_origin/rv5_downloads/**/*']
        },
        unzip: {
            highlight: {
                src: ['build/rv5_images.zip', 'build/rv5_downloads.zip'],
                dest: 'src/'
            }
        },
        replace: {
            local: {
                src: ['src/**/*.html', 'src/_js/**/*.js', 'src/_scss/**/*.scss'],
                overwrite: true,
                replacements: [{
                    from: /http:\/\/usarmy.vo.llnwd.net\/e2\/(?!rv5_js\/3rdparty|rv5_js\/main)/g,
                    to: 'http://localhost:8282/to_origin/'
                }, {
                    from: 'http://frontend.ardev.us/development/<%= pkg.name %>/to_origin/',
                    to: 'http://localhost:8282/to_origin/'
                }]
            },
            cdn: {
                src: ['src/**/*.html', 'src/_js/**/*.js', 'src/_scss/**/*.scss'],
                overwrite: true,
                replacements: [{
                    from: 'http://localhost:8282/to_origin/',
                    to: 'http://usarmy.vo.llnwd.net/e2/'
                }, {
                    from: 'http://frontend.ardev.us/development/<%= pkg.name %>/to_origin/',
                    to: 'http://usarmy.vo.llnwd.net/e2/'
                }, {
                    from: 'http://frontend.ardev.us/api/',
                    to: 'http://www.army.mil/api/'
                }]
            },
            dev: {
                src: ['src/**/*.html', 'src/_js/**/*.js', 'src/_scss/**/*.scss'],
                overwrite: true,
                replacements: [{
                    from: /http:\/\/usarmy.vo.llnwd.net\/e2\/(?!rv5_js\/3rdparty|rv5_js\/main)/g,
                    to: 'http://frontend.ardev.us/development/<%= pkg.name %>/to_origin/'
                }, {
                    from: 'http://localhost:8282/to_origin/',
                    to: 'http://frontend.ardev.us/development/<%= pkg.name %>/to_origin/'
                }, {
                    from: 'http://www.army.mil/api/',
                    to: 'http://frontend.ardev.us/api/'
                }]
            }
        },
        'http-server': {
            'dev': {
                root: 'src/',
                port: 8282,
                host: "0.0.0.0",
                ext: "html",
                runInBackground: false
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
                mangle: true,
                compress: true,
                beautify: false
            },
            build: {
                src: [
                    'src/_js/ranger/get_news.js',
                    'src/_js/ranger/social.js',
                ],
                dest: 'src/to_origin/rv5_js/ranger/<%= pkg.name %>.min.js'
            }
        },
        'sftp-deploy': {
            build: {
                auth: {
                    host: 'frontend.ardev.us',
                    authKey: 'privateKey'
                },
                cache: 'sftpCache.json',
                src: 'src/',
                dest: '/www/development/<%= pkg.name %>',
                exclusions: ['build/', 'node_module/', 'Gruntfile.js', 'package.json', 'readme.md', '.sass-cache', '.git', '.gitignore'],
                serverSep: '/',
                concurrency: 4,
                progress: true
            }
        }
    });


    grunt.loadNpmTasks('grunt-sftp-deploy');

    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.loadNpmTasks('grunt-http-server');

    grunt.loadNpmTasks('grunt-text-replace');

    grunt.loadNpmTasks('grunt-curl');

    grunt.loadNpmTasks('grunt-zip');

    grunt.loadNpmTasks('grunt-accessibility');

    grunt.loadNpmTasks('grunt-jslint');

    grunt.loadNpmTasks("grunt-jsbeautifier");

    grunt.loadNpmTasks('grunt-contrib-jshint');

    grunt.loadNpmTasks('grunt-contrib-sass');

    grunt.registerTask('default', ['images', 'local', 'http-server']);

    grunt.registerTask('dev', ['replace:dev', 'sass']);

    grunt.registerTask('local', ['replace:local', 'sass']);

    grunt.registerTask('cdn', ['replace:cdn', 'sass']);

    grunt.registerTask('images', ['curl', 'unzip']);

};
